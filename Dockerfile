FROM docker.io/debian:bullseye
LABEL name="docker-tang"
LABEL url="https://gitlab.com/thomasdstewart-infra/docker-tang"
LABEL maintainer="thomas@stewarts.org.uk"

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install socat tang \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

VOLUME /var/lib/tang
EXPOSE 8080
USER _tang:_tang
CMD ["socat", "tcp-l:8080,reuseaddr,fork", "exec:'/usr/libexec/tangd /var/lib/tang'"]
