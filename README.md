# Tang Docker Container on Debian
Built nightly and avaiable here: registry.gitlab.com/thomasdstewart-infra/docker-tang:latest

## Podman
* podman build -t tang .
* mkdir $(pwd)/tang-data
* uidgid=$(podman run tang getent passwd _tang | awk -F: '{print $3 ":" $4}')
* podman unshare chown $uidgid -R $(pwd)/tang-data
* podman run -d -p 8080:8080 -v $(pwd)/tang-data:/var/lib/tang tang

## Docker
* docker build -t tang .
* mkdir $(pwd)/tang-data
* docker run -d -p 8080:8080 -u $(id -u) -v $(pwd)/tang-data:/var/lib/tang tang

## Usage
* echo "Hello World..." | clevis encrypt tang '{ "url": "http://localhost:8080"}' > secret.jwe
* base64 -d < secret.jwe | jq
* clevis decrypt < secret.jwe

#
docker build -t registry.stewarts.org.uk/docker-tang:amd64 --build-arg ARCH=amd64 .
docker push registry.stewarts.org.uk/docker-tang:amd64

docker build -t registry.stewarts.org.uk/docker-tang:arm32v7 --build-arg ARCH=arm32v7 --platform linux/arm/v7 .
docker push registry.stewarts.org.uk/docker-tang:arm32v7

docker build --platform linux/arm/v8 -t registry.stewarts.org.uk/docker-tang:arm64v8 --build-arg ARCH=arm64v8 .
docker push registry.stewarts.org.uk/docker-tang:arm64v8

docker manifest create registry.stewarts.org.uk/docker-tang:latest \
--amend registry.stewarts.org.uk/docker-tang:amd64 \
--amend registry.stewarts.org.uk/docker-tang:arm32v7 \
--amend registry.stewarts.org.uk/docker-tang:arm64v8

OR
docker manifest create registry.stewarts.org.uk/docker-tang:latest registry.stewarts.org.uk/docker-tang:amd64 registry.stewarts.org.uk/docker-tang:arm32v7 
docker manifest annotate registry.stewarts.org.uk/docker-tang:latest registry.stewarts.org.uk/docker-tang:arm32v7 --os linux --arch arm --variant v7

docker manifest push registry.stewarts.org.uk/docker-tang:latest


https://www.docker.com/blog/multi-arch-build-and-images-the-simple-way/
